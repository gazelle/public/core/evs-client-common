package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.processing.OtherCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.ProxyCallerMetadata;

public class CallerMetadataFactory {

    public EVSCallerMetadata getCallerMetadata(String toolOid, String externalId, String proxyType, EntryPoint entryPoint) {
        if(proxyType != null){
            return new ProxyCallerMetadata(entryPoint, toolOid, externalId, proxyType);
        } else if(toolOid != null && externalId != null) {
            return new OtherCallerMetadata(entryPoint, toolOid, externalId);
        } else {
            return new EVSCallerMetadata(entryPoint);
        }
    }
}
