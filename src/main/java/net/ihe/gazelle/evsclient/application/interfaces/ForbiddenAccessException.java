package net.ihe.gazelle.evsclient.application.interfaces;

public class ForbiddenAccessException extends Exception {
   private static final long serialVersionUID = 5973173406101459609L;

   public ForbiddenAccessException(String s) {
      super(s);
   }
}
