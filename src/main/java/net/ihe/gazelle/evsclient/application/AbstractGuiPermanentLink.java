package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;

public abstract class AbstractGuiPermanentLink<T extends AbstractProcessing> implements GuiPermanentLink<T> {

    protected ApplicationPreferenceManager applicationPreferenceManager;

    protected AbstractGuiPermanentLink(ApplicationPreferenceManager applicationPreferenceManager) {
        this.applicationPreferenceManager = applicationPreferenceManager;
    }

    public abstract String getResultPageUrl();

    public String getPermanentLink(T selectedObject) {
        final StringBuilder builder = new StringBuilder();
        builder.append(applicationPreferenceManager.getApplicationUrl().replaceFirst("[/]+$",""))
                .append("/")
                .append(getResultPageUrl().replaceFirst("^[/]+",""))
                .append("?").append(QueryParam.PROCESSING_OID).append("=").append(selectedObject.getOid());
        if (selectedObject.getPrivacyKey() != null && !selectedObject.getPrivacyKey().isEmpty()) {
            builder.append("&").append(QueryParam.PRIVACY_KEY).append("=").append(selectedObject.getPrivacyKey());
        }
        return builder.toString();
    }

}
