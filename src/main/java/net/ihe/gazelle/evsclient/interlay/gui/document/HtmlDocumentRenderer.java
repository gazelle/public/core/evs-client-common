package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.uwyn.jhighlight.renderer.XhtmlRendererFactory;
import org.jsoup.Jsoup;
import org.jsoup.safety.Cleaner;
import org.jsoup.safety.Whitelist;

import java.io.IOException;
import java.nio.charset.Charset;

public class HtmlDocumentRenderer extends AbstractDocumentRenderer<AbstractDocumentRenderer.Options> {
    public HtmlDocumentRenderer() {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        Cleaner cleaner = new Cleaner(Whitelist.relaxed());
        return cleaner.clean(
                Jsoup.parse(
                        toString(doc))
        ).toString();
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        try {
            return XhtmlRendererFactory.getRenderer("xhtml")
                    .highlight("xhtml", message, encoding.name(), true);
        } catch (IOException e) {
            throw new RenderingException(e);
        }
    }

}
