package net.ihe.gazelle.evsclient.interlay.gui.document;

import com.pixelmed.dicom.AttributeList;
import net.ihe.gazelle.dicom.evs.api.Pixelmed;
import org.dcm4che2.tool.dcm2txt.Dcm2Txt;

import javax.xml.transform.TransformerConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class Pixelmed2txtDumper extends DicomDumper {
    @Override
    protected String dump(String dicomFilePath) throws Exception {
        AttributeList list = new AttributeList();
        list.read(dicomFilePath);
        String a = list.toString();
        String[] listLign = a.split("\n");
        StringBuffer toView = new StringBuffer();
        String tab = "";

        for(int i = 0; i < listLign.length; ++i) {
            if (!listLign[i].startsWith("%seq") && !listLign[i].startsWith("%item")) {
                if (listLign[i].startsWith("%end")) {
                    if (tab.length() > 3) {
                        tab = tab.substring(0, tab.length() - 4);
                    }
                } else {
                    toView = toView.append(tab + listLign[i] + "\n");
                }
            } else {
                tab = tab + "----";
            }
        }
        return toView.toString().replaceAll("\u0000","");
    }

    @Override
    protected String name() {
        return "Pixelmed";
    }
}
