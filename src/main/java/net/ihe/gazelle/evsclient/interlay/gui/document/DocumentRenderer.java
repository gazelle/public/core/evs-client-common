package net.ihe.gazelle.evsclient.interlay.gui.document;

public interface DocumentRenderer<O extends DocumentRenderer.Options> extends Renderer<String, O> {
    interface Options extends Renderer.Options {
        Boolean hasViewLineNumberMode();

        Boolean hasPrettyViewMode();

        Boolean hasMaxColumnsMode();

        Boolean getViewLineNumber();

        Boolean getPrettyView();
    }

}
