package net.ihe.gazelle.evsclient.interlay.gui;

public class QueryParam {

    public static final String PROCESSING_ID = "id";
    public static final String PROCESSING_OID = "oid";
    public static final String PRIVACY_KEY = "privacyKey";

    public static final String TOOL_OID = "toolOid";
    public static final String EXTERNAL_ID = "externalId";
    public static final String PROXY_TYPE = "proxyType";

    public static final String KEY = "key";
    public static final String LUCKY = "lucky";

    public static final String VALIDATION_REF = "validationRef";

    public static final String CALLER_PRIVACY_KEY = "callerPrivacyKey";


    protected QueryParam(){}

}
