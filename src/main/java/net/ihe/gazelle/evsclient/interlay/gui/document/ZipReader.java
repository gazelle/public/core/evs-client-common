package net.ihe.gazelle.evsclient.interlay.gui.document;

import java.io.*;
import java.util.zip.ZipInputStream;

public class ZipReader {

    public ZipReader() {
        super();
    }

    public byte[] readZipEntry(byte[] bytes, String entry) throws IOException {
        return readZipEntry(
                new ZipInputStream(new ByteArrayInputStream(bytes)),
                entry
        );
    }

    public byte[] readZipEntry(File file, String entry) throws IOException {
        return readZipEntry(
                new ZipInputStream(new BufferedInputStream(new FileInputStream(file))),
                entry
        );
    }

    public byte[] readZipEntry(ZipInputStream zis, String entry) throws IOException {
        return new ContentConverter().protect(
                zipExtractFile(
                        seekZipEntry(zis,entry)
                )
        );
    }

    private ZipInputStream seekZipEntry(ZipInputStream zis, String entry) throws IOException {
        String entryName=null;
        while (!entry.equals(entryName)) {
            entryName = zis.getNextEntry().getName();
        }
        return zis;
    }

    public static byte[] zipExtractFile(ZipInputStream zipIn) throws IOException {
        ByteArrayOutputStream bos ;
        bos = new ByteArrayOutputStream();
        byte[] bytesIn = new byte[4096];
        int read;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
        return bos.toByteArray();
    }

}
