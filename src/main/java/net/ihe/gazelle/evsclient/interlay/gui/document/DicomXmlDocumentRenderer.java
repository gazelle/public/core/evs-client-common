package net.ihe.gazelle.evsclient.interlay.gui.document;

import javax.faces.model.SelectItem;
import java.util.Arrays;
import java.util.List;

public class DicomXmlDocumentRenderer extends XmlDocumentRenderer {

    private DicomDumperSelector selector;

    public DicomXmlDocumentRenderer() {
        super();
        this.selector = new DicomDumperSelector(Arrays.asList(new Pixelmed2xmlDumper(), new Dcm2xmlDumper()));
        this.options.hasRenderingModes = true;
        this.options.renderingMode = new Pixelmed2xmlDumper().name();
    }

    public List<SelectItem> getRenderingModes() {
        return selector.getDumpingModes();
    }

    @Override
    public String getStyleClass() {
        return XmlDocumentRenderer.class.getSimpleName();
    }

    @Override
    protected String toString(HandledDocument doc) throws RenderingException {
        return ""; // cannot render binary dicom
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        return selector.dump(this,doc);
    }


}
