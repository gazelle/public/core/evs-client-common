package net.ihe.gazelle.evsclient.interlay.gui;

import org.jboss.seam.core.ResourceBundle;

import java.text.MessageFormat;

public class I18n {
    public static String get(String key) {
        try {
            return key == null ? "" : ResourceBundle.instance().getString(key);
        } catch (java.util.MissingResourceException|java.lang.IllegalStateException e) {
            return key;
        }
    }
    public static String get(String key, Object ... params) {
        return MessageFormat.format(get(key),params);
    }
    private I18n() {}
}
