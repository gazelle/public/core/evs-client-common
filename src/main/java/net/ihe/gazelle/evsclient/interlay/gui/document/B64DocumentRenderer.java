package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.jfree.util.Log;

import java.nio.charset.Charset;
import java.util.Map;

public class B64DocumentRenderer extends AbstractDocumentRenderer<AbstractDocumentRenderer.Options> {
    public B64DocumentRenderer() {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
        this.options.hasViewLineNumberMode = false;
        this.options.hasPrettyViewMode = true;
        this.options.viewLineNumber = false;
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String render(HandledDocument doc,
                         final Map<Integer, String> listErrors,
                         final Map<Integer, String> listWarnings) {
        try {
            return "<embed src=\"data:"+doc.getMimeType()+";base64,"+toString(doc)+"\" type=\""+doc.getMimeType()+"\" width=\"100%\" height=\"100%\"/>";
        } catch (Exception e) {
            String warningMessage = String.format("Unable to render %s : %s", doc.getSyntax(), e.getMessage());
            Log.warn(warningMessage, e);
            return warningMessage;
        }
    }

    protected String toString(HandledDocument doc) throws RenderingException {
        // do not decode B64
        return doc.toString();
    }
}
