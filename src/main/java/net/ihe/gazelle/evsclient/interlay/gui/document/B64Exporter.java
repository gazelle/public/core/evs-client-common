package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.util.DocumentFileUpload;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import java.io.*;

public class B64Exporter {


    public void showOrDownload(boolean download, String filename, File f) throws IOException {
        InputStream is = null;
        try {
            is = new FileInputStream(f.getAbsoluteFile());
            DocumentFileUpload.showFile(is, filename, download);
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    public File toTempFile(String b64, String prefix, String suffix) throws IOException {
        return toTempFile(Base64.decodeBase64(b64),prefix,suffix);
    }
    public File toTempFile(byte[] bytes, String prefix, String suffix) throws IOException {
        File f;
        f = createTempFile(prefix, suffix);
        if (bytes!=null && bytes.length>0) {
            OutputStream os = null;
            try {
                os = new FileOutputStream(f);
                os.write(bytes);
            } finally {
                IOUtils.closeQuietly(os);
            }
        }
        return f;
    }
    private File createTempFile(final String prefix, String suffix) {
        try {
            return File.createTempFile(prefix, suffix);
        } catch (Exception e) {
            return null;
        }
    }
}
