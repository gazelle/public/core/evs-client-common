package net.ihe.gazelle.evsclient.interlay.gui.document;

public interface ArchiveLister {
    String list();
}
