package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.evsclient.interlay.util.SystemExecutor;

import javax.xml.transform.TransformerConfigurationException;
import java.io.File;
import java.io.IOException;

public class Dicom3tools2txtDumper extends DicomDumper {

    private SystemExecutor executor;

    public Dicom3tools2txtDumper() {
        this.executor = new SystemExecutor();
    }

    @Override
    protected String dump(String dicomFilePath) throws TransformerConfigurationException, IOException, InterruptedException {
        return executor.exec(getBinaryLocation()+" "+dicomFilePath,true);
    }

    @Override
    protected String name() {
        return "Dicom3tools";
    }

    @Override
    protected boolean isAvailable() {
        return new File(getBinaryLocation()).canExecute();
    }

    private String getBinaryLocation() {
        return executor.getBinaryLocation("dcdump","/opt/dicom3tools");
    }


}
