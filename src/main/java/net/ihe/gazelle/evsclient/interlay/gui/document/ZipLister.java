package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipLister implements ArchiveLister {
    public static SortedSet<ZipEntry> list(ZipInputStream zis) throws IOException {
        ZipEntry ze;
        SortedSet<ZipEntry> entries = new TreeSet<>(new Comparator<ZipEntry>() {
            @Override
            public int compare(ZipEntry a, ZipEntry b) {
                return a.getName().compareTo(b.getName());
            }
        });
        while ((ze = zis.getNextEntry()) != null) {
            entries.add(ze);
        }
        return entries;
    }

    private byte[] bytes;

    public ZipLister(byte[] bytes) {
        this.bytes = bytes;
    }

    public String list() {
        StringBuilder sb = new StringBuilder();
        try {
            ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(bytes));
            for (ZipEntry x : list(zis)) {
                sb.append(MessageFormat.format("{1}\t{2,date}\t{0}\n",
                        x.getName(),
                        readableFileSize(x.getSize()),
                        new Date(x.getTime())));
            }
        } catch (Exception e) {
            return e.getMessage();
        }
        return sb.toString();
    }

    private String readableFileSize(long size) {
        final String[] units = new String[]{"B ", "kB", "MB", "GB", "TB"};
        int digitGroups = size <= 0 ? 0 : (int) (Math.log10(size) / Math.log10(1024));
        return StringUtils.leftPad(new DecimalFormat("# ##0.#").format(size <= 0 ? 0 : size / Math.pow(1024, digitGroups)), 7) + units[digitGroups];
    }

}
