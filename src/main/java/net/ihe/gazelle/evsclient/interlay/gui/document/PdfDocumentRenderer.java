package net.ihe.gazelle.evsclient.interlay.gui.document;

import org.jfree.util.Log;

import java.nio.charset.Charset;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;

public class PdfDocumentRenderer extends AbstractDocumentRenderer<AbstractDocumentRenderer.Options> {
    public PdfDocumentRenderer() {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
        this.options.hasViewLineNumberMode = false;
        this.options.hasPrettyViewMode = false;
        this.options.viewLineNumber = false;
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String render(HandledDocument doc,
                         final Map<Integer, String> listErrors,
                         final Map<Integer, String> listWarnings) {
        try {
            return "<embed src=\"data:application/pdf;base64,"+Base64.encodeBase64String(doc.getContent())+"\" type=\"application/pdf\" width=\"100%\" height=\"100%\"/>";
        } catch (Exception e) {
            String warningMessage = String.format("Unable to render %s : %s", doc.getSyntax(), e.getMessage());
            Log.warn(warningMessage, e);
            return warningMessage;
        }
    }
}
