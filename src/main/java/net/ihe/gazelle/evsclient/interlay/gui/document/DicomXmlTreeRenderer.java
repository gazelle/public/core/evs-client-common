package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.common.tree.XmlNodeData;

import javax.faces.model.SelectItem;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class DicomXmlTreeRenderer extends XmlTreeRenderer {

    private DicomDumperSelector selector;
    private String lastRendering;

    public DicomXmlTreeRenderer() {
        super();
        this.selector = new DicomDumperSelector(Arrays.asList(new Pixelmed2xmlDumper(), new Dcm2xmlDumper()));
        this.options.hasRenderingModes = true;
        this.options.renderingMode = new Pixelmed2xmlDumper().name();
    }

    public List<SelectItem> getRenderingModes() {
        return selector.getDumpingModes();
    }

    @Override
    protected GazelleTreeNodeImpl<XmlNodeData> toTreeNode(HandledDocument doc) throws RenderingException {
        try {
            if (this.lastRendering != options.renderingMode) {
                this.tree = null;
            }
            return super.toTreeNode(
                    new HandledDocument(
                            selector.dump(this,doc)
                                    .getBytes(StandardCharsets.UTF_8)));
        } catch(Exception e) {
            throw new RenderingException(e);
        } finally {
            this.lastRendering = options.renderingMode;
        }
    }

    @Override
    public String getStyleClass() {
        return XmlTreeRenderer.class.getSimpleName();
    }
}
