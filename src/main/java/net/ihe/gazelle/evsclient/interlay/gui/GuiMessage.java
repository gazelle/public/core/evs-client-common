package net.ihe.gazelle.evsclient.interlay.gui;

import net.ihe.gazelle.evsclient.interlay.util.ExceptionUtil;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.owasp.html.Sanitizers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;

public class GuiMessage {

    private static final Logger LOGGER = LoggerFactory.getLogger(GuiMessage.class);

    private static final String JSTL_REGEX = "[\\$#]\\{.*";

    private static final Pattern JSTL_PATT = Pattern.compile(JSTL_REGEX);

    private GuiMessage() {}

    public static void logMessage(StatusMessage.Severity severity, String message){
        logMessage(severity, message, null);
    }

    public static void logMessage(StatusMessage.Severity severity, String message, Throwable e){
        switch (severity) {
            case FATAL:
            case ERROR:
                LOGGER.error(I18n.get(message), e);
                break;
            case WARN:
                LOGGER.warn(I18n.get(message), e);
                break;
            case INFO:
            default:
                LOGGER.info(I18n.get(message), e);
                break;
        }

        String sanitizedMessage = Sanitizers.FORMATTING.and(Sanitizers.LINKS).sanitize(I18n.get(message) + ExceptionUtil.getMessageAndCauses(e));
        sanitizedMessage = sanitizeJSTL(sanitizedMessage);
        FacesMessages.instance().add(severity, sanitizedMessage);
    }

    private static String sanitizeJSTL(String message) {
        return JSTL_PATT.matcher(message).replaceAll("");
    }


}
