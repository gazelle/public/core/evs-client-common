package net.ihe.gazelle.evsclient.interlay.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;

public interface Repository {

   /**
    * Build a specific file path for saving a resource of a processingObject
    *
    * @param applicationPreferenceManager preference of the applications (used to load repository root path)
    * @param id id of the processingObject for building a specific file name.
    * @return an absolute file path for the given processingObject.
    * @throws IllegalStateException if the repository root is not defined in the applications' preferences.
    */
   String buildFilePath(ApplicationPreferenceManager applicationPreferenceManager, String id);

   /**
    * Build a file path for saving a resource of a processingObject
    *
    * @param applicationPreferenceManager preference of the applications (used to load repository root path)
    * @return an absolute file path in the repository.
    * @throws IllegalStateException if the repository root is not defined in the applications' preferences.
    */
   String buildFilePath(ApplicationPreferenceManager applicationPreferenceManager);

   /**
    * Get the application's perference key used to store the repository root path.
    * @return the preference key.
    */
   String getRootPathPreferenceKey();

   class DatePath {

      private DatePath(){}

      public static String build() {
         final Date date = new Date();
         final Calendar cal = Calendar.getInstance();
         cal.setTime(date);
         final String year = Integer.toString(cal.get(Calendar.YEAR));
         final String month = Integer.toString(cal.get(Calendar.MONTH) + 1);
         final String day = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
         return year + '/' + month + '/' + day;
      }
   }

   class Path {
      private static final String REPOSITORY_DATED_PATH_TEMPLATE = "{0}/{4}/{1}{3}{2}";
      private static final String REPOSITORY_PLAIN_PATH_TEMPLATE = "{0}/{1}{2}";

      private Path(){}

      public static String build(String beginPath, String filePrefix, String fileExtension, String id) {
         return MessageFormat.format(id == null ? REPOSITORY_PLAIN_PATH_TEMPLATE : REPOSITORY_DATED_PATH_TEMPLATE,
               beginPath,
               filePrefix,
               fileExtension,
               id,
               DatePath.build());
      }
   }
}
