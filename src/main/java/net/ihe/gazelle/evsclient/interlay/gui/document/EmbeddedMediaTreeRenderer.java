package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.common.tree.XmlNodeData;

import java.util.NoSuchElementException;

public class EmbeddedMediaTreeRenderer extends XmlTreeRenderer {

    public EmbeddedMediaTreeRenderer() {
        super();
        this.options.hasFilterMode=false;
        this.options.hasExpandAllMode = false;
        this.options.actions.validation = false;
        this.nodeFilter="//observationMedia/value[@mediaType='application/pdf'][@representation='B64']/..";
    }

    @Override
    protected String serialize(GazelleTreeNodeImpl<XmlNodeData> node) {
        StringBuilder sb = new StringBuilder();
        XmlNodeData data = node.getData();
        if (data!=null) {
            GazelleTreeNodeImpl<XmlNodeData> mediaNode = getMediaNode(node);
            if (mediaNode!=null) {
                sb.append(downloadFilename(node));
            }
        }
        return sb.toString();
    }

    @Override
    protected GazelleTreeNodeImpl<XmlNodeData> getMediaNode(GazelleTreeNodeImpl<XmlNodeData> node) {
        try {
            GazelleTreeNodeImpl<XmlNodeData> mediaNode = (GazelleTreeNodeImpl<XmlNodeData>) node.getChild(node.getChildrenKeysIterator().next());
            if (mediaNode != null && mediaNode.getData().getAttributes().containsKey("mediaType")) {
                return mediaNode;
            }
        } catch (NoSuchElementException nsee) {
            return null;
        }
        return null;
    }

    @Override
    public String downloadFilename(GazelleTreeNodeImpl<XmlNodeData> node) {
        GazelleTreeNodeImpl<XmlNodeData> valueNode = getMediaNode(node);
        return node.getData().getAttributes().get("ID")+"."+valueNode.getData().getAttributes().get("mediaType").toString().replaceFirst(".*/([^/]+)$", "$1");
    }

    @Override
    public Boolean isRenderedNode(GazelleTreeNodeImpl<XmlNodeData> node) {
        if (node!=null && node.isLeaf()) {
            return false;
        }
        return super.isRenderedNode(node);
    }
}
