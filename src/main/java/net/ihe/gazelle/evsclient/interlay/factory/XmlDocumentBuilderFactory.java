package net.ihe.gazelle.evsclient.interlay.factory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * XmlDocumentBuilderFactory is a factory for creating DocumentBuilder objects.
 * By default, the DocumentBuilder produced by this class will disallow the use of DTDs to prevent XXE attacks.
 * All configuration options could be set by using the setters.
 */

public class XmlDocumentBuilderFactory {

    public static final String ERROR_CREATING_DOCUMENT_BUILDER = "Error creating document builder";
    private final DocumentBuilderFactory documentBuilderFactory;



    public XmlDocumentBuilderFactory() {
        documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl",true);
            documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            documentBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        } catch (ParserConfigurationException e) {
            throw new DocumentBuilderFactoryException(ERROR_CREATING_DOCUMENT_BUILDER, e);
        }
    }

    public DocumentBuilder getBuilder()  {
        try {
            return documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new DocumentBuilderFactoryException(ERROR_CREATING_DOCUMENT_BUILDER, e);
        }
    }

    public XmlDocumentBuilderFactory setFeature(String name, boolean value) {
        try {
            documentBuilderFactory.setFeature(name, value);
        } catch (ParserConfigurationException e) {
            throw new DocumentBuilderFactoryException(ERROR_CREATING_DOCUMENT_BUILDER, e);
        }
        return this;
    }

    public XmlDocumentBuilderFactory setNamespaceAware(boolean value) {
        documentBuilderFactory.setNamespaceAware(value);
        return this;
    }

    public XmlDocumentBuilderFactory setIgnoringComments(boolean value) {
        documentBuilderFactory.setIgnoringComments(value);
        return this;
    }

}
