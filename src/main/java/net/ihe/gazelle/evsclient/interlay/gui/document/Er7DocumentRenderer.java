package net.ihe.gazelle.evsclient.interlay.gui.document;

import net.ihe.gazelle.tm.utils.GazelleUtilHighlightForHL7v2;

import java.nio.charset.Charset;

public class Er7DocumentRenderer extends AbstractDocumentRenderer {
    public Er7DocumentRenderer() {
        super(new AbstractDocumentRenderer.Options(true),AbstractDocumentRenderer.Options.class);
    }

    @Override
    protected String prettify(HandledDocument doc) throws RenderingException {
        return toString(doc);
    }

    @Override
    protected String highlight(String message, Charset encoding) throws RenderingException {
        return GazelleUtilHighlightForHL7v2.highlightForHL7v2Message(
                message
        );
    }
}
