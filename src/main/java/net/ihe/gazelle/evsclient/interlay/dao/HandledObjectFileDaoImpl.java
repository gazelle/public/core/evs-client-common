package net.ihe.gazelle.evsclient.interlay.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.interlay.util.FileUtil;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

class HandledObjectFileDaoImpl {

   private ApplicationPreferenceManager applicationPreferenceManager;

   HandledObjectFileDaoImpl(ApplicationPreferenceManager applicationPreferenceManager) {
      this.applicationPreferenceManager = applicationPreferenceManager;
   }

   @Deprecated
   HandledObject loadContentFromFile(HandledObjectFile object) throws NotLoadedException {
      try {
         object.setContent(FileUtils.readFileToByteArray(new File(object.getFilePath())));
         return object;
      } catch (IOException e) {
         throw new NotLoadedException(e);
      }
   }

   /**
    * Save content of the HandledObjects on File System.
    *
    * @param processing AbstractProcessing containing a list of HandledObjects to save.
    * @param repository Repository to save the files.
    *
    * @return the AbstractProcessing (The HandledObjects may have been proxied)
    *
    * @throws ProcessingNotSavedException in case of file I/O exception or repository definition error.
    */
   AbstractProcessing saveObjectsInFiles(AbstractProcessing processing, Repository repository)
         throws ProcessingNotSavedException {
      if (processing.getObjects() != null) {
         for (int i = 0; i < processing.getObjects().size(); i++) {
            HandledObjectFile object = proxyingAsHandledObjectFile(processing, i);
            if (isHandledObjectNotYetSaved(object)) {
               saveContentInFile(object, repository, processing.getId().toString() + "_" + i);
            }
         }
      }
      return processing;
   }

   /**
    * DELETE associated file
    *
    * @param o
    *
    * @throws IOException
    */
   void deleteContentFile(HandledObjectFile o) throws IOException {
      if (o.getFilePath() != null) {
         Files.deleteIfExists(Paths.get(o.getFilePath()));
      }
   }

   private HandledObjectFile proxyingAsHandledObjectFile(AbstractProcessing processing, int i) {
      HandledObjectFile object = new HandledObjectFile(processing.getObjects().get(i));
      processing.getObjects().set(i, object);
      return object;
   }

   private boolean isHandledObjectNotYetSaved(HandledObjectFile object) {
      // HandledObject previously saved will always be relaoded by the persistance layer as an HandledObjectFile with a
      // defined filePath.
      return object.getFilePath() == null;
   }

   private void saveContentInFile(HandledObjectFile object, Repository repository, String fileId)
         throws ProcessingNotSavedException {
      try {
         object.setFilePath(repository.buildFilePath(applicationPreferenceManager, fileId));
         FileUtil.createFileAndParentDirectories(object.getContent(), object.getFilePath());
         if (object.getOriginalFileName() == null || object.getOriginalFileName().isEmpty()) {
            String fileName = object.getFilePath().substring(object.getFilePath().lastIndexOf("/") + 1,object.getFilePath().lastIndexOf(".") - 1);
            object.setOriginalFileName(fileName);
         }
      } catch (IllegalStateException | IOException e) {
         throw new ProcessingNotSavedException("Unable to write object content in file", e);
      }
   }


}
