package net.ihe.gazelle.evsclient.interlay.gui.processing;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.interlay.gui.Labelizer;
import net.ihe.gazelle.evsclient.interlay.gui.document.HandledDocument;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import java.util.Locale;

public abstract class FileProcessingBeanGui extends DocumentProcessingBeanGui {

    private boolean defaultShowMessageInGui;
    protected String uploadedFileName;

    private boolean showMessageAnalyzerInGui;
    private boolean analyzeUploadedMessage;

    protected FileProcessingBeanGui(Class<?> cls) {
        super(cls);
        try {
            this.defaultShowMessageInGui = ApplicationPreferenceManagerImpl.instance().getBooleanValue("show_uploaded_message");
        } catch (Exception e) {
            this.defaultShowMessageInGui = true;
        }
        setShowMessageInGui(defaultShowMessageInGui);

        try {
            this.showMessageAnalyzerInGui = ApplicationPreferenceManagerImpl.instance().getBooleanValue("show_uploaded_message_analyzer");
        } catch (Exception e) {
            this.showMessageAnalyzerInGui = false;
        }
        try {
            this.analyzeUploadedMessage = ApplicationPreferenceManagerImpl.instance().getBooleanValue("analyze_uploaded_message");
            if (this.analyzeUploadedMessage) {
                showMessageAnalyzerInGui = true;
            }
        } catch (Exception e) {
            this.analyzeUploadedMessage = false;
        }
    }

    public String getUploadedFileName() {
        if (uploadedFileName==null) {
            if (document == null) {
                return "";
            } else {
                return "manual-edited-content." + document.getSyntax().name().toLowerCase(Locale.ROOT);
            }
        } else {
            return uploadedFileName;
        }
    }

    public String getUploadedFileNameWithoutExtension() {
        int indexOfExtension = getUploadedFileName().lastIndexOf('.');
        return indexOfExtension>0?getUploadedFileName().substring(0, indexOfExtension):"";
    }

    public void setUploadedFileName(String uploadedFileName) {
        this.uploadedFileName = uploadedFileName;
    }

    public byte[] getUploadedFileContent() {
        return getDocumentContent();
    }

    public void resetUpload() {
        this.reset();
    }

    public Boolean needsUpload() {
        return (getUploadedFileContent() == null);
    }

    @Override
    public void reset() {
        super.reset();
        this.uploadedFileName = null;
    }

    public String getUploadedFileContentType() {
        return document ==null?null: document.getMimeType();
    }

    public void uploadListener(final FileUploadEvent event) {
        final UploadedFile uploadedFile = event.getUploadedFile();
        if (uploadedFile.getData() != null && uploadedFile.getData().length > 0) {
            setUploadedFileName(uploadedFile.getName());
            setDocument(new HandledDocument(uploadedFile.getData()));
            setShowMessageInGui(
                    defaultShowMessageInGui &&
                    presenter.isDisplayableContentType()
            );
        } else {
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.ERROR, "file is empty");
        }
    }

    public boolean getShowMessageAnalyzerInGui() {
        return showMessageAnalyzerInGui;
    }

    public void setShowMessageAnalyzerInGui(boolean showMessageAnalyzerInGui) {
        this.showMessageAnalyzerInGui = showMessageAnalyzerInGui;
    }

    public boolean getAnalyzeUploadedMessage() {
        return analyzeUploadedMessage;
    }

    public void setAnalyzeUploadedMessage(boolean analyzeUploadedMessage) {
        this.analyzeUploadedMessage = analyzeUploadedMessage;
    }
}
