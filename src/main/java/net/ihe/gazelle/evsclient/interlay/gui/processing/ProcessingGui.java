package net.ihe.gazelle.evsclient.interlay.gui.processing;

import net.ihe.gazelle.evsclient.interlay.gui.Labelizer;

public abstract class ProcessingGui {

    private String code;
    private String id;
    private String label;
    protected Labelizer labelizer;
    protected Class<?> cls;

    public ProcessingGui(Class<?> cls) {
        this.cls = cls;
        this.labelizer = new Labelizer("net.ihe.gazelle.evs.");
    }

    public String getLabel() {
        if (this.label==null) {
            this.label = labelizer.getLabel(getCode());
        }
        return this.label;
    }

    public String getId() {
        if (this.id==null) {
            this.id = getCode()+hashCode();
        }
        return this.id;
    }

    public String getCode() {
        if (this.code==null) {
            this.code = labelizer.getCode(cls.getSimpleName());
        }
        return this.code;
    }

    public String getLabel(String value, Object ... params) {
        return labelizer.getLabel(value,params);
    }
    public String getLabel(String value) {
        return labelizer.getLabel(value);
    }
    public String getCode(String value) {
        return labelizer.getCode(value);
    }

}
