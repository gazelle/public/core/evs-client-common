package net.ihe.gazelle.evsclient.interlay.dao;

import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import org.apache.commons.io.FileUtils;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@Entity
@DiscriminatorValue("File")
public class HandledObjectFile extends HandledObject {
    private static final long serialVersionUID = -2961015036351569815L;

    @Column(name = "file_path")
    private String filePath;

    HandledObjectFile() {
    }

    public HandledObjectFile(byte[] content) {
        super(content);
    }

    public HandledObjectFile(byte[] content, String originalFileName) {
        super(content, originalFileName);
    }

    public HandledObjectFile(HandledObject handledObject) {
        setId(handledObject.getId());
        setOriginalFileName(handledObject.getOriginalFileName());
        setRole(handledObject.getRole());
        if(handledObject instanceof HandledObjectFile) {
            // if true, then content should NOT be copied for performance issue as a
            // call to getContent() will trigger a read from the file system.
            // Copying the filePath is enough.
            this.filePath = ((HandledObjectFile) handledObject).getFilePath();
        } else {
            setContent(handledObject.getContent());
        }
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public byte[] getContent() {
        if(super.getContent() == null && filePath != null) {
            try {
                setContent(FileUtils.readFileToByteArray(new File(filePath)));
            } catch (IOException e) {
                throw new FileReadException("Unable to load content from file.", e);
            }
        }
        return super.getContent();
    }

    @Override
    public String toString() {
        return String.format("HandledObjectFS{id='%s', content='%s', originalFileName='%s', filePath='%s'}",
              getId(), Arrays.toString(getContent()), getOriginalFileName(), getFilePath());
    }

}
