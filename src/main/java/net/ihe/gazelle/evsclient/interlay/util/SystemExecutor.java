package net.ihe.gazelle.evsclient.interlay.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class SystemExecutor {
    public String getBinaryLocation(String cmd, String defaultLocation) {
        try {
            return exec("which " + cmd, false).replaceFirst("\n*$", "");
        } catch (Exception e) {
            return new File(defaultLocation).toPath().resolve(cmd).toAbsolutePath().toString();
        }
    }

    public String exec(String cmd, boolean stderr) throws IOException, InterruptedException {
        StringBuffer result = new StringBuffer();
        BufferedReader reader = null;
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(cmd);
            reader = new BufferedReader(new InputStreamReader(stderr ? proc.getErrorStream() : proc.getInputStream(), StandardCharsets.UTF_8.name()));
            String s;
            while ((s = reader.readLine()) != null) {
                result.append(s).append("\n");
            }
            return result.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }
}
