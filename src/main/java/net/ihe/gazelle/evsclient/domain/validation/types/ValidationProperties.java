package net.ihe.gazelle.evsclient.domain.validation.types;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidationProperties {
    String name();
    ValidationType[] types();
}
