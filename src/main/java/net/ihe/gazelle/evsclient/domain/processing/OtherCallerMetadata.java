package net.ihe.gazelle.evsclient.domain.processing;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "evsc_other_caller_metadata")
public class OtherCallerMetadata extends EVSCallerMetadata {

    /**
     * Identifies the tool from which the file comes (if not EVSClient itself)
     */
    @Column(name = "tool_oid")
    private String toolOid;

    @Column(name = "tool_object_id")
    private String toolObjectId;

    public OtherCallerMetadata() {
    }

    public OtherCallerMetadata(EntryPoint entryPoint, String toolOid, String toolObjectId) {
        super(entryPoint);
        this.toolOid = toolOid;
        this.toolObjectId = toolObjectId;
    }

    public String getToolOid() {
        return toolOid;
    }

    public void setToolOid(String toolOid) {
        this.toolOid = toolOid;
    }

    public String getToolObjectId() {
        return toolObjectId;
    }

    public void setToolObjectId(String toolObjectId) {
        this.toolObjectId = toolObjectId;
    }

    @Override
    public String toString() {
        return "OtherCallerMetadata{" +
                "toolOid='" + toolOid + '\'' +
                ", toolObjectId='" + toolObjectId + '\'' +
                '}';
    }
}
