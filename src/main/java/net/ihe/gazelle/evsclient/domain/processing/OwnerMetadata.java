package net.ihe.gazelle.evsclient.domain.processing;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "evsc_owner_metadata")
@SequenceGenerator(name = "owner_metadata_sequence", sequenceName = "evsc_owner_metadata_id_seq", allocationSize = 1)
public class OwnerMetadata implements Serializable {

    private static final long serialVersionUID = 1152809725609400304L;

    @Id
    @GeneratedValue(generator = "owner_metadata_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "user_ip")
    private String userIp;

    @Column(name = "username")
    private String username;

    @Column(name = "organization")
    private String organization;

    public OwnerMetadata() {
    }

    public OwnerMetadata(String userIp, String username, String organization) {
        this.userIp = userIp;
        this.username = username;
        this.organization = organization;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "OwnerMetadata{" +
                "id=" + id +
                ", userIp='" + userIp + '\'' +
                ", username='" + username + '\'' +
                ", organization='" + organization + '\'' +
                '}';
    }
}
