package net.ihe.gazelle.evsclient.domain.processing;

import net.ihe.gazelle.evsclient.interlay.gui.I18n;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Arrays;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "discriminator", discriminatorType = DiscriminatorType.STRING)
@Table(name = "evsc_handled_object")
@SequenceGenerator(name = "handled_object_sequence", sequenceName = "evsc_handled_object_id_seq", allocationSize = 1)
public class HandledObject implements Serializable {

   private static final long serialVersionUID = 345836906394456131L;

   @Id
   @GeneratedValue(generator = "handled_object_sequence", strategy = GenerationType.SEQUENCE)
   @Column(name = "id")
   private Integer id;

   @Transient
   private byte[] content;

   @Column(name = "original_file_name")
   private String originalFileName;

   @Column(name = "role")
   protected String role;

   public HandledObject() {
   }

   public HandledObject(byte[] content) {
      setContent(content);
   }

   public HandledObject(byte[] content, String originalFileName) {
      this(content);
      this.originalFileName = originalFileName;
   }

   public HandledObject(byte[] content, String originalFileName, String role) {
      this(content, originalFileName);
      this.role = role;
   }

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public byte[] getContent() {
      return content;
   }

   public void setContent(byte[] content) {
      if (content == null || content.length == 0) {
         throw new IllegalArgumentException(I18n.get("net.ihe.gazelle.document.empty-content"));
      }
      this.content = content;
   }

   public String getOriginalFileName() {
      return originalFileName;
   }

   public void setOriginalFileName(String originalFileName) {
      this.originalFileName = originalFileName;
   }

   public String getRole() {
      return role;
   }

   public void setRole(String role) {
      this.role = role;
   }

   @Override
   public String toString() {
      return "HandledObject{" +
            "originalFileName='" + originalFileName + '\'' +
            ", role='" + role + '\'' +
            ", content=" + Arrays.toString(content) +
            '}';
   }
}
